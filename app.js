'use strict'

let fragment = document.createDocumentFragment();
const items = document.getElementById('items');
const footer = document.getElementById('footer');
let card_articulos = document.getElementById('card_articulos');
let template_articulos = document.getElementById('template_articulos').content;
let template_carrito = document.getElementById('template_carrito').content;
let template_footer = document.getElementById('template_footer').content;
let carrito = {};


// --------------------- GET ARTICULOS ---------------------
let get_articulos = async () => {
    try {
        let res = await fetch('api.json');
        let data = await res.json();
        mostrar_articulos(data);
    }
    catch (e) {
        throw e;
    }
}

document.addEventListener("DOMContentLoaded", () => { // DOMContentLoaded, se ejecuta cuando la pagina carga completamente
    get_articulos();

    if (localStorage.getItem('carrito')){

        carrito = JSON.parse(localStorage.getItem('carrito')); // JSON.parse => pasar texto plano a JSON
        mostrar_carrito();
    }

})
items.addEventListener("click", e => {
    btn_accion(e);
})


// --------------------- MOSTRAR ARTICULOS ---------------------

let mostrar_articulos = data => {

    data.forEach(item => {
        
        template_articulos.querySelector('img').setAttribute('src', item.thumbnailUrl); // setAttribute => si NO existe el atributo lo crea, de lo contratio lo reemplaza 
        template_articulos.querySelector('.card-text').textContent =  item.precio;
        template_articulos.querySelector('.title').textContent = item.title;
        template_articulos.querySelector('.btn').dataset.id = item.id; // create attribute data-id in html

        const clone_template_articulos = template_articulos.cloneNode(true); //cloneNode => crear clone de cada template-articulos y guardarlo
        fragment.appendChild(clone_template_articulos);
    })

    card_articulos.appendChild(fragment);
}

// --------------------- ADD CARRITO ---------------------
card_articulos.addEventListener("click", e => {

    if (e.target.classList.contains('btn')) {
        add_carrito(e.target.parentElement); // parentElement => devuelve el padre del elemento
    } 
    e.stopPropagation();
})

const add_carrito = obj => {
    
    const producto = {
        id: obj.querySelector('.btn').dataset.id,
        title: obj.querySelector('.title').textContent,
        precio: obj.querySelector('p').textContent,
        cantidad: 1
    }
    if (carrito.hasOwnProperty(producto.id)) { // preguntamos si existe un producto con ese id?
        producto.cantidad = carrito[producto.id].cantidad + 1; // set cantidad
    } 
    carrito[producto.id] = {...producto} // ... => te ingresa solamente el contenido del objeto
    mostrar_carrito();
}


// --------------------- MOSTRAR CARRITO ---------------------
const mostrar_carrito = () => {

    items.innerHTML = ''
    const clone_footer = template_footer.cloneNode(true);
    fragment.appendChild(clone_footer);
    footer.appendChild(fragment);
    
    Object.values(carrito).forEach( producto => { // Object.values => para poder usar el objeto como un arreglo
        template_carrito.querySelector('th').textContent = producto.id;
        template_carrito.querySelectorAll('td')[0].textContent = producto.title;
        template_carrito.querySelectorAll('td')[1].textContent = producto.cantidad;
        template_carrito.querySelector('.btn-info').dataset.id = producto.id;
        template_carrito.querySelector('.btn-danger').dataset.id = producto.id;
        template_carrito.querySelector('span').textContent = producto.cantidad * producto.precio;

        const clone = template_carrito.cloneNode(true);
        fragment.appendChild(clone);
    })
    localStorage.setItem('carrito',  JSON.stringify(carrito)) // json.stringify => parse to json

    items.appendChild(fragment);
    mostrar_footer();
} 

// --------------------- MOSTRAR FOOTER ---------------------
const mostrar_footer = () => {

    footer.innerHTML = ''

    if (Object.keys(carrito).length === 0) { // si no existen elementos
        footer.innerHTML = '<th scope="row" colspan="5">Carrito vacío - comience a comprar!</th>';
        return
    }
    // let example = [ {'cantidad':4} ,{'cantidad':6}].reduce( (acumulador, {cantidad}) => acumulador+cantidad,0); // out_put = 10
    const cantidad_t = Object.values(carrito).reduce((acc,{cantidad}) => acc + cantidad,0 ) // sumara la cantidad de los objetos del carrito
    const precio_t = Object.values(carrito).reduce((acc,{cantidad,precio}) => acc + (cantidad * precio),0) // obtenemos el precio total

    template_footer.querySelectorAll('td')[0].textContent = cantidad_t;
    template_footer.querySelector('span').textContent = precio_t;

    const clone_footer = template_footer.cloneNode(true);
    fragment.appendChild(clone_footer);
    footer.appendChild(fragment);

    const btn_vaciar = document.getElementById('vaciar_carrito');
    btn_vaciar.addEventListener('click', e => {
        carrito = {}
        mostrar_carrito();
    })
    
 }

 // -------------------------------- BTN ACCION --------------------------------
 const btn_accion = e => {
     // aumentar cantidad
     if (e.target.textContent == '+') {
        carrito[e.target.dataset.id].cantidad++;       
     } 
     // disminuir cantidad
     else if (e.target.textContent == '-'){
        carrito[e.target.dataset.id].cantidad--;
        if (carrito[e.target.dataset.id].cantidad === 0){
            delete carrito[e.target.dataset.id]; // delete item
        }  

     }
     e.stopPropagation();
     mostrar_carrito();
 }

